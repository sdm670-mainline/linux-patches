This is a repository for patches to the [kernel for SDM670](https://gitlab.com/sdm670-mainline/linux).

# Background

Patches to the Linux kernel are not allowed to reflect the history of
development, but should contain one logical change each. In the beginning of
the SDM670 kernel, commits were amended with any changes needed to be suitable
for upstream using `git commit --amend`. Additionally, patches are rebased for
every kernel release to clearly indicate which patches are downstream in the
git logs. This made it difficult to follow or participate in the development of
the kernel fork, since the git history in the kernel repository is overwritten.

This repository was created to let others easily follow and participate in the
development of the SDM670 kernel, while patches stay "ready" to be submitted
and amendments can be squashed semi-automatically. Without the need to keep
patches in the form of constantly amended commits on the kernel repository,
commits can reflect the development history since the previous release, with
the use of tags in the commit descriptions to specify the intended organization
of changes into patches.

# Contributing

You may either contribute to this repository containing patches or to the
repository containing the [patched kernel](https://gitlab.com/sdm670-mainline/linux)
at your convenience.

There are some additional tags used here for scripts to synchronize the patches
and patched kernel. Each commit should have up to one tag, if applicable:

    Amends: contains the exact subject line of the patch that this makes substantial changes to
    Parent: contains the exact subject line of the patch immediately before the added patch

Examples:

    arm64: dts: qcom: sdm670: remove unused lpi pinctrl settings
    
    This description in an amendment will not be sent to the upstream kernel,
    so it can be as informal, short, or chaotic as permitted under applicable
    codes of conduct (namely the ones from Gitlab and Alpine).
    
    This amendment applies to a patch where the SDM670 repositories are the
    canonical sources for it (i.e. the patch is not cherry picked). Amendments
    should not be made for patches that are cherry-picked, but a new patch
    should be added instead with the changes.
    
    Amends: arm64: dts: qcom: sdm670: add lpi pinctrl
    Signed-off-by: Richard Acayan <mailingradian@gmail.com>

    arm64: dts: qcom: sdm670-google: fix style
    
    The upstream kernel normally doesn't accept changes that are purely
    semantic. The description here will eventually matter when it is submitted
    to the kernel.
    
    Parent: arm64: dts: qcom: sdm670-google: enable gpu
    Signed-off-by: Richard Acayan <mailingradian@gmail.com>

If you wish to contribute to the kernel repository, please put up to one
`Amends:` tag per commit.

## Imported patches

Patches that have been cherry-picked from other repositories (except for
amendments that get squashed between releases) need to be kept up-to-date by
the maintainers. These patches should not be modified unless it is infeasible
to make any of the same modifications in a separate patch.

In the patch description, imported patches should have the "(cherry picked from
commit ...)" line appended first and a Signed-off-by tag appended last. If they
have been modified (such as by fixing merge conflicts), there should be a line
in between to detail the modifications.

Example:

    drm/msm/dpu: deduplicate some (most) of SSPP sub-blocks
    
    As we have dropped the variadic parts of SSPP sub-blocks declarations,
    deduplicate them now, reducing memory cruft.
    
    Reviewed-by: Abhinav Kumar <quic_abhinavk@quicinc.com>
    Signed-off-by: Dmitry Baryshkov <dmitry.baryshkov@linaro.org>
    Patchwork: https://patchwork.freedesktop.org/patch/570112/
    Link: https://lore.kernel.org/r/20231201234234.2065610-7-dmitry.baryshkov@linaro.org
    (cherry picked from commit 0fd205412e1ee1f60f549269838119969f0883ad)
    [richard: also remove sdm670 sub-blocks to resolve merge conflict]
    Signed-off-by: Richard Acayan <mailingradian@gmail.com>

You should link to the source repository where the latest updates to the patch
are to be made. This is normally the kernel-related repository where the author
is most involved in, although it may be different, such as when the MSM8939
kernel stopped being maintained in favour of the MSM8916 kernel, leading to the
MSM8916 community maintaining the q6voice patches.

When the patch is from the MSM8916, MSM8996, SDM845, or linux-next kernels,
they should be put with other patches from the same repository, sorted by date
committed. The release notes should give a rough idea of where these patches
are in the series.

When the patch is from a different kernel repository, a link to it should be
provided with the contribution.

# Maintenance

These are guidelines for maintaining this repository and using write access
responsibly.

## Release schedule

A release of the fork is tagged after a major release is merged into the
[linux-rolling-stable](https://git.kernel.org/pub/scm/linux/kernel/git/stable/linux.git/log/?h=linux-rolling-stable)
repository. This normally happens after the merge window closes for the next
release.

Example timeline:

- 27 Aug 2023 - v6.5 tagged
- 10 Sept 2023 - v6.6-rc1 tagged
- 13 Sept 2023 - v6.5.3 tagged and merged into linux-rolling-stable
- 13 Sept 2023 - beta release sdm670-v6.5.3\_beta1 tagged
- 23 Sept 2023 - tested release sdm670-v6.5.3 tagged

## Rebases

Force pushes should only occur when rebasing onto a new release. A rebase
should happen once after a new major release is merged into
`linux-rolling-stable`.

## Patch updates

Some patches are from other repositories, and need to be kept up-to-date. This
should happen before a rebase, unless there are conflicts or the patches are
updated after the rebase.

Each patch is from a canonical repository where amendments may be made. The
patch should be cherry-picked from the latest release tag that contains it.
